import React, { Component } from 'react';
import './App.css';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import {withRouter} from "react-router-dom";
import { Paper } from '@material-ui/core';
import { Query } from 'react-apollo';
import gql from 'graphql-tag';
import LinearProgress from '@material-ui/core/LinearProgress';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + (theme.spacing.unit * (3 * 2)))]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    width: 128,
    height: 128,
  },
  form: {
    margin: theme.spacing.unit * 8,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  textButton: {
    marginTop: theme.spacing.unit,
  },
  textButtonContainer: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'column',
  },
})

const GET_USERS = gql`
  {
    users{
      name
      password
    }
  }
`;

class Dashboard extends Component {
  state ={
    email: '',
    password: '',
  }

  onChangeText = (key, value) => {
    this.setState({ [key]: value.trim()  })
  }

  logout = () => {
    localStorage.removeItem('token')
    this.props.history.push('/signIn');
  }

  ListItem = (data) => {
    const { classes } = this.props
    return (
        <Card className={classes.card}>
          <CardContent>
            <Typography className={classes.title} variant="title">
              Email: {data.name}
            </Typography>
            <Typography className={classes.pos} variant="body2">
              Password: {data.password}
            </Typography>
          </CardContent>
        </Card>
    );
  }

  render() {
  const { classes } = this.props;
    
    return (
      <Query query={GET_USERS} fetchPolicy="no-cache" >
      {
        ({loading, error, data}) => {
          if(loading) return <LinearProgress/>
          console.log(data)
          return (
            <Paper>
              <Button
              type="submit"
              fullWidth
              variant="raised"
              color="primary"
              className={classes.submit}
              onClick={this.logout}
              >
              Sign out
              </Button>
              {
                data.users.map(data => this.ListItem(data))
              }
            </Paper>
          )
        }
      }
      </Query>
    );
  }
}

export default withStyles(styles)(withRouter(Dashboard));