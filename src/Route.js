import React from 'react'
import { Switch, Route, withRouter } from 'react-router-dom'
import PrivareRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import SignInScreen from './SignIn'
import DashboardScreen from './Dashboard'

const Routes = () => (
 <main> 
  <Switch>
    <PublicRoute exact path="/signIn" component={SignInScreen} />
    <PrivareRoute exact path="/" component={DashboardScreen} />
  </Switch>  
</main>
);

export default withRouter(Routes)